using System;
using System.Collections.Generic;
using System.Text;

namespace Colecoes
{
    class Pessoa
    {
        public int id {set; get;}
        public string nome {set; get;}
        public int idade {set; get;}
        public char sexo {set; get;}
        public Pessoa()
        {}
 
        public Pessoa(int id, string nome, int idade, char sexo)
        {
            this.id    = id;
            this.nome  = nome;
            this.idade = idade;
            this.sexo  = sexo;
        }        
    }
}
