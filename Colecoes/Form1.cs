﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Colecoes
{
    public partial class Form1 : Form
    {
        List<Pessoa> pLista;

        void Imprimir(List<Pessoa> pLista, string info)
        {
            lstResultado.Items.Clear();
            lstResultado.Items.Add(info);
            lstResultado.Items.Add("");
            lstResultado.Items.Add("ID\tNome\tIdade\tSexo");
            foreach (Pessoa p in pLista)
            {
                lstResultado.Items.Add(p.id+ "\t"+ p.nome+"\t"+ p.idade + "\t" + p.sexo);
            }            
        }
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            carregaLista();
            Imprimir(pLista, "Lista ordenada por ID");
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            
        }

        private void button2_Click(object sender, EventArgs e)
        {
            List<Pessoa> listaFiltroPorIdade = pLista.FindAll(delegate(Pessoa p) 
            { 
                return p.idade > 30; 
            });
            Imprimir(listaFiltroPorIdade, "Pessoas com idades maiore que 30.");

        }
        private void carregaLista()
        {
            pLista = new List<Pessoa>();
            pLista.Add(new Pessoa(1, "João", 29, 'M'));
            pLista.Add(new Pessoa(2, "Macoratti", 35, 'F'));
            pLista.Add(new Pessoa(3, "Americo", 25, 'M'));
            pLista.Add(new Pessoa(4, "Katia", 21, 'F'));
            pLista.Add(new Pessoa(5, "Lena", 33, 'F'));
            pLista.Add(new Pessoa(6, "Suzana", 45, 'F'));
            pLista.Add(new Pessoa(7, "Jim", 38, 'M'));
            pLista.Add(new Pessoa(8, "Jane", 32, 'F'));
            pLista.Add(new Pessoa(9, "Roberto", 31, 'M'));
            pLista.Add(new Pessoa(10, "Cintia", 25, 'F'));
            pLista.Add(new Pessoa(11, "Gina", 27, 'F'));
            pLista.Add(new Pessoa(12, "Joel", 33, 'M'));
            pLista.Add(new Pessoa(13, "Germano", 55, 'M'));
            pLista.Add(new Pessoa(14, "Ricardo", 22, 'M'));
            pLista.Add(new Pessoa(15, "Maria", 39, 'F'));
        }

        private void button3_Click(object sender, EventArgs e)
        {
            List<Pessoa> ordenaListaPorNome = pLista;
            ordenaListaPorNome.Sort(delegate(Pessoa p1, Pessoa p2)
            {
                return p1.nome.CompareTo(p2.nome);
            });
            Imprimir(ordenaListaPorNome, "Lista ordenada por Nome");
        }

       private void button5_Click(object sender, EventArgs e)
        {
            List<Pessoa> removerLista = pLista;
            removerLista.RemoveAll(delegate(Pessoa p) 
            { 
                return p.sexo == 'M'; 
            });
            Imprimir(removerLista, "Removendo da lista pessoas do sexo masculino");
        }

        private void button6_Click(object sender, EventArgs e)
        {
            Pessoa pessoa = pLista.Find(delegate(Pessoa p1)
            {
                return p1.id == 10;
            }
            );
            lstResultado.Items.Add("Pessoa localizada na lista com ID = 10");
            lstResultado.Items.Add(pessoa.id+ "\t"+ pessoa.nome+"\t"+ pessoa.idade + "\t" + pessoa.sexo);
        }

        private void button4_Click(object sender, EventArgs e)
        {
            //Instanciando uma nova lista
            List<Pessoa> pessoas = new List<Pessoa>();
            Pessoa p;
            
            p = new Pessoa(1, "João", 5, 'M');
            //Adicionar o objeto na lista
            pessoas.Add(p);

            p = new Pessoa(2, "Maria", 4, 'F');
            //Adicionar o objeto na lista
            pessoas.Add(p);

            p = new Pessoa(3, "Joaquim", 3, 'M');
            //Adicionar o objeto na lista
            pessoas.Add(p);

            //Verificando o número de elementos em uma lista
            MessageBox.Show(pessoas.Count.ToString());

            //Percorrendo a lista
            MessageBox.Show("Lista desordenada");
            foreach (Pessoa p1 in pessoas)
            {
                MessageBox.Show(p1.nome + " - " + p1.idade.ToString());
            }

            //Ordenando a lista por nome
            MessageBox.Show("Lista ordenada por nome");
            pessoas.Sort(delegate(Pessoa p1, Pessoa p2)
            {
                return p1.nome.CompareTo(p2.nome);
            });
            foreach (Pessoa p1 in pessoas)
            {
                MessageBox.Show(p1.nome + " - " + p1.idade.ToString());
            }

            //Ordenando a lista por idade
            MessageBox.Show("Lista ordenada por idade");
            pessoas.Sort(delegate(Pessoa p1, Pessoa p2)
            {
                return p1.idade.CompareTo(p2.idade);
            });
            foreach (Pessoa p1 in pessoas)
            {
                MessageBox.Show(p1.nome + " - " + p1.idade.ToString());
            }

            //Localizar um nome na lista
            Pessoa pLoc = pessoas.Find(delegate(Pessoa p1)
            {
                return p1.nome == "Maria";
            });
            if (pLoc != null)
            {
                MessageBox.Show("Nome encontrado");
            }
            else
            {
                MessageBox.Show("Nome não encontrado");
            }            
        }        
    }
}